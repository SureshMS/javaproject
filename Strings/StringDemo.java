package com.mypackage;

public class StringDemo {

	public static void main(String[] args) {
		String s1 = new String("chennai");
		String s2 = "Chennai";
		String s3 = "chennai";
		String s4 = new String("madurai");
		String s5 = "india";
        System.out.println(s3.equalsIgnoreCase(s2));
		System.out.println(s1);
		s1 = s1.concat(" tamil nadu");
		System.out.println(s1);
        s2 = s2.concat(" south india");
		System.out.println(s2);
		s3 = s3.concat( s5);
		System.out.println(s3);
        System.out.println(s1==s2);
        System.out.println(s3.compareTo(s2));
        
	}

}
