package com.mypackage;

public class SavingsAccount extends Account {
	 
	 public SavingsAccount(String id, double initialDeposit)
	 {
	 super(id,initialDeposit);//call Account class constructor
	 }
	 @Override
	 public boolean withdraw(double amount) {
	  if (balance > amount) 
	  {
	   balance = balance-amount;
	    return true ;
	  }
	  else
	     
	  return false;
	 }
	 @Override
	 public void deposit(double amount) {
	  balance = balance + amount;
	 }
	 
	 public double addInterest(double rate)
	 {
	  double interest = (balance * rate)/100 ;
	  balance = balance + interest;
	  return balance;
	  
	}
	}
